package ru.psu.mobileapp.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import java.util.*

/********************************************************************************************************
 * Базовый класс для всех объектов модели.                                                              *
 * @author Селетков И.П. 2018 0816.                                                                     *
 *******************************************************************************************************/
abstract class CObjectBase
(
    /****************************************************************************************************
     * Идентификатор.                                                                                   *
     ***************************************************************************************************/
    @PrimaryKey(autoGenerate                = false)
    @Json(name                              = "id")
    @ColumnInfo(name                        = "id")
    var id                                  : UUID
                                            = UUID.randomUUID()
)