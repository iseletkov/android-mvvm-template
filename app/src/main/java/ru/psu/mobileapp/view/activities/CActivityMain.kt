package ru.psu.mobileapp.view.activities

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerAppCompatActivity
import ru.psu.mobileapp.R
import javax.inject.Inject
import ru.psu.mobileapp.viewmodel.CViewModelActivityMain


class CActivityMain                         : DaggerAppCompatActivity()
{

    @Inject
    lateinit var viewModelFactory           : ViewModelProvider.Factory
    private lateinit var viewModel          : CViewModelActivityMain

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel                           = ViewModelProviders.of(this, viewModelFactory).get(CViewModelActivityMain::class.java)


    }
}
