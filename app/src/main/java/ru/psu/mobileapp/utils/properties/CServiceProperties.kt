package ru.psu.mobileapp.utils.properties

import android.content.Context
import java.io.FileOutputStream
import java.util.*

import javax.inject.Inject
import javax.inject.Singleton

/********************************************************************************************************
 * Класс описывает логику работы с параметрами приложения.                                              *
 * @author Селетков И.П. 2019 0211.                                                                     *
 *******************************************************************************************************/
@Singleton
class CServiceProperties
/********************************************************************************************************
 * Конструктор.                                                                                         *
 * @param context - контекст выполнения приложения.                                                     *
 *******************************************************************************************************/
@Inject constructor
(
    private val context                     : Context
)                                           : IServiceProperties
{
    //Получение ссылки на файл с параметрами с указанным именем.
    private val preferences                 = context.getSharedPreferences("ru.psu.mobileapp.mvvm", Context.MODE_PRIVATE)
    private val defaults                    = Properties()

    init {
        defaultProperties()
    }

    /****************************************************************************************************
     * Создаём настройки по-умолчанию.                                                                  *
     ***************************************************************************************************/
    private fun defaultProperties()
    {
        defaults["ru.psu.mobileapp.mvvm.API.address"]= "https://localhost:8080"
    }

    /****************************************************************************************************
     * Возвращает параметр по указанному адресу.                                                        *
     * @param key - ключ записи в файле xml.                                                            *
     *            Например, "ru.psu.mobileapp.mvvm.API.address".                                        *
     ***************************************************************************************************/
    override fun get(key                    : String)
                                            : String?
    {
        return preferences.getString(key, defaults[key] as String?)
    }
    /****************************************************************************************************
     * Задаёт значение параметра.                                                                       *
     * @param key - ключ записи в файле xml.                                                            *
     *            Например, "ru.psu.mobileapp.mvvm.API.address".                                        *
     * @param value - новое значение параметра.                                                         *
     ***************************************************************************************************/
    override fun set(
        key                                 : String,
        value                               : String)
    {
        with (preferences.edit()) {
            putString(key, value)
            apply()
        }
        return
    }
}
