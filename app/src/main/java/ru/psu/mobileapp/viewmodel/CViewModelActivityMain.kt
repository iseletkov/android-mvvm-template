package ru.psu.mobileapp.viewmodel

import androidx.lifecycle.MutableLiveData
import ru.psu.mobileapp.data.services.IServiceSubject
import ru.psu.mobileapp.model.CSubject
import javax.inject.Inject

/********************************************************************************************************
 * Модель основного представления.                                                                      *
 * @author Селетков И.П. 2018 0925.                                                                     *
 *******************************************************************************************************/
class CViewModelActivityMain
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor
(
    val serviceSubject                      : IServiceSubject
)                                           : CViewModelBase()
{
    //Список дисциплин, отображаемых на главной активности.
    val subjects                            = MutableLiveData<List<CSubject>>()
    //Признак того, что происходит актуализация данных.
    //Влияет на видимость индикатора.
    val loading                             = MutableLiveData<Boolean>()

    //Начальные значения всех полей.
    init
    {
        subjects.postValue(ArrayList())
        loading.value                       = false
    }
}