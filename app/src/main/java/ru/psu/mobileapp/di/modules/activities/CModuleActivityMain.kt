package ru.psu.mobileapp.di.modules.activities

import dagger.Module
import dagger.Provides
import ru.psu.mobileapp.view.activities.CActivityMain

/********************************************************************************************************
 * Модуль dagger 2, отвечает за хранение и внедрение сылок на объекты главной активности.               *
 * @author Селетков И.П. 2018 0920.                                                                     *
 *******************************************************************************************************/
@Module
class CModuleActivityMain
{
    @Provides
    fun provideActivityMain(
        activityMain                        : CActivityMain
    )                                       : CActivityMain
    {
        return activityMain
    }
}