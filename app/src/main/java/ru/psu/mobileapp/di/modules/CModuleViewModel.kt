package ru.psu.mobileapp.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Binds
import dagger.multibindings.IntoMap
import ru.psu.mobileapp.viewmodel.CViewModelActivityMain
import ru.psu.mobileapp.viewmodel.CViewModelFactory
import ru.psu.mobileapp.viewmodel.CViewModelKey

/********************************************************************************************************
 * Модуль Dagger позволяет внедрять зависимости на модели представлений.                                *
 * @author Селетков И.П. 2018 0830.                                                                     *
 *******************************************************************************************************/
@Module
@Suppress("unused")
abstract class CModuleViewModel
{
    @Binds
    internal abstract fun bindViewModelFactory(
        factory                             : CViewModelFactory
    )                                       : ViewModelProvider.Factory

    /****************************************************************************************************
     * Подготавливает модель представления для главной активности.                                      *
     * @return модель списка обходов.                                                                   *
     ***************************************************************************************************/
    @Binds
    @IntoMap
    @CViewModelKey(CViewModelActivityMain::class)
    abstract fun bindsViewModelActivityMain(
        viewModelActivityMain               : CViewModelActivityMain
    )                                       : ViewModel
}