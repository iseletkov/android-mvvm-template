package ru.psu.mobileapp.di.modules

import dagger.Binds
import dagger.Module
import ru.psu.mobileapp.data.services.CServiceSubject
import ru.psu.mobileapp.data.services.IServiceSubject
import ru.psu.mobileapp.model.CSubject

/********************************************************************************************************
 * Модуль Dagger позволяет внедрять ссылки на сервисы работы с данными.                                 *
 * @author Селетков И.П. 2018 0818.                                                                     *
 *******************************************************************************************************/
@Module
@Suppress("unused")
abstract class CModuleDataService
{
    /****************************************************************************************************
     * Возвращает ссылку на сервис для работы с данными типа [CSubject].                                *
     * @return сервис для работы с данными.                                                             *
     ***************************************************************************************************/
    @Binds
    abstract fun serviceSubject(
        service                             : CServiceSubject
    )                                       : IServiceSubject
}