package ru.psu.mobileapp.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.psu.mobileapp.di.modules.activities.CModuleActivityMain
import ru.psu.mobileapp.di.scopes.CScopeActivity
import ru.psu.mobileapp.view.activities.CActivityMain

@Module
abstract class CModuleActivityBinding
{
//    @CScopeActivity
//    @ContributesAndroidInjector(
//        modules                             = [
//            CModuleActivityLauncher::class
//        ]
//    )
//    internal abstract fun bindActivityLauncher(
//
//    )                                       : CActivityLauncher

    @CScopeActivity
    @ContributesAndroidInjector(
        modules                             = [
            CModuleActivityMain::class
        ]
    )
    internal abstract fun bindActivityMain(
    )                                       : CActivityMain



}