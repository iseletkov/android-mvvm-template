package ru.psu.mobileapp.di.modules

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.psu.mobileapp.CApplication
import ru.psu.mobileapp.utils.properties.CServiceProperties
import ru.psu.mobileapp.utils.properties.IServiceProperties
import javax.inject.Singleton

/********************************************************************************************************
 * Модуль Dagger позволяет внедрять ссылки на объект-приложение в места, где это запрашивается.         *
 * https://medium.com/@marco_cattaneo/integrate-dagger-2-with-room-persistence-library-in-              *
 * few-lines-abf48328eaeb                                                                               *
 * @author Селетков И.П. 2018 0816.                                                                     *
 *******************************************************************************************************/
@Module
abstract class CModuleApplication {

    @Binds
    abstract fun provideContext(
        app                                 : CApplication
    )                                       : Context

    /****************************************************************************************************
     * Возвращает объект для работы с параметрами приложения с установленными в него зависимостями.     *
     * @return объект для логирования.                                                                  *
     ***************************************************************************************************/
    @Binds
    abstract fun provideProperties(
        properties                          : CServiceProperties
    )                                       : IServiceProperties
}